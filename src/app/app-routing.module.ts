import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListaDeReceitasComponent} from './components/lista-de-receitas/lista-de-receitas.component';
import {ReceitaDetailComponent} from './components/receita-detail/receita-detail.component';
import {CadastroComponent} from './components/cadastro/cadastro.component'
import { EditarComponent } from './components/editar/editar.component';
const routes: Routes = [
  { path: '', redirectTo: '/lista', pathMatch: 'full' },
  { path: 'lista', component: ListaDeReceitasComponent },
  { path: 'detalhes/:id', component: ReceitaDetailComponent },
  { path: 'cadastro', component: CadastroComponent},
  { path: 'editar/:id', component: EditarComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }