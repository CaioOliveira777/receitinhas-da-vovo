import { Component, OnInit } from '@angular/core';
import { Receita } from 'src/app/receita';
import { Location } from '@angular/common';
import { ReceitaService } from 'src/app/receita.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {
receita: Receita | undefined;
  constructor( 
    private receitaService: ReceitaService,
    private location: Location,
    private toastr: ToastrService
    ) {    }

  ngOnInit(): void {
  }

  goBack(): void {
    this.location.back();
  }

  onClick(titulo: HTMLInputElement, descricao: HTMLInputElement, img: HTMLInputElement): void {
    if(titulo.value.length > 0 && descricao.value.length > 0 && img.value.length > 0) {
      this.receitaService.addReceita(titulo.value,descricao.value,img.value);
      this.toastr.success('Você cadastrou uma receita!', 'Parabéns!', {
        timeOut:1300
      });
      setTimeout(() => {
        this.location.back();
      },1300) 
    } else {
       if(titulo.value.length === 0){ 
         this.toastr.error('Você deixou o título em branco', 'Operação inválida', {
         timeOut:3000
       });
     }
     if(descricao.value.length === 0){ 
       this.toastr.error('Você deixou a descrição em branco', 'Operação inválida', {
       timeOut:3000
     });
   }
   if(img.value.length === 0){ 
     this.toastr.error('Você deixou a imagem em branco', 'Operação inválida', {
     timeOut:3000
   });
 }
    }
    
  }
}
