import { Component, OnInit } from '@angular/core';
import { Receita } from 'src/app/receita';
import { Location } from '@angular/common';
import { ReceitaService } from 'src/app/receita.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {
receita: Receita | undefined;
  constructor(
    private route: ActivatedRoute,
    private receitaService: ReceitaService,
    private location: Location,
    ) { }

  ngOnInit(): void {
    this.getReceita();
  }
  goBack(): void {
    this.location.back();
  }
  getReceita(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.receitaService.getReceita(id).subscribe(receita=> this.receita = receita);
  }
  onSalvar():void {
    this.location.back();
  }
}
