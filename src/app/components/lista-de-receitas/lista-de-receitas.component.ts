import { Component, OnInit } from '@angular/core';
import { ReceitaService } from 'src/app/receita.service';
import { Receita } from 'src/app/receita';

@Component({
  selector: 'app-lista-de-receitas',
  templateUrl: './lista-de-receitas.component.html',
  styleUrls: ['./lista-de-receitas.component.css']
})
export class ListaDeReceitasComponent implements OnInit {
confirmar: boolean = false;
receitas: Receita[] = [];

  constructor(private receitaService: ReceitaService) { }

  ngOnInit(): void {
    this.getReceitas();
  }
getReceitas(): void {
  this.receitaService.getReceitas().subscribe(receitas => this.receitas = receitas);
}

onDelete(indexA:number): void {
  this.confirmar = confirm('VOCÊ QUER MESMO DELETAR A RECEITA?? Vovó vai ficar triste 😭');
  if(this.confirmar) {
    this.receitaService.deleteReceita(indexA);
} else {
  this.confirmar=false;
}
}
}
