import { Component, OnInit } from '@angular/core';
import { Receita } from 'src/app/receita';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ReceitaService } from 'src/app/receita.service';

@Component({
  selector: 'app-receita-detail',
  templateUrl: './receita-detail.component.html',
  styleUrls: ['./receita-detail.component.css']
})
export class ReceitaDetailComponent implements OnInit {
receita: Receita | undefined;
  constructor(
    private route: ActivatedRoute,
    private receitaService: ReceitaService,
    private location: Location
    ) { }

  ngOnInit(): void {
    this.getReceita();
  }

  getReceita(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.receitaService.getReceita(id).subscribe(receita=> this.receita = receita);
  }
  goBack(): void {
    this.location.back();
  }
}
