import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Receita } from './receita'; 
import { RECEITAS } from './receitas-mocadas';

@Injectable({
  providedIn: 'root'
})
export class ReceitaService {

  constructor() { }

  getReceitas(): Observable<Receita[]> {
    const receitas = of(RECEITAS);
    return receitas;
  }

  getReceita(id:number): Observable<Receita> {
    const receita = RECEITAS.find(r => r.id === id)!;
    return of(receita);
  }

  deleteReceita(deleteIndex: number): void{
    RECEITAS.splice(deleteIndex, 1);
  }

  addReceita(titulo: string, descricao:string, img: string): void {
    let id:number;
    let tamanho: number = RECEITAS.length;
    
    
    if (tamanho === 0){
     id = 1;
     } else {
       id= RECEITAS[tamanho-1].id + 1;
     }
   
    RECEITAS.push({id, titulo, descricao, img});
  }
}
