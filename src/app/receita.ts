export interface Receita {
    id: number;
    titulo: string;
    descricao: string;
    img: string;
}